package android.exercise.mini.interactions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EditTitleActivity extends AppCompatActivity {

  //defines if the text is currently being edited
  private boolean isEditing = false;
  private String currentTitle = "Page title here";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_title);

    // find all views
    FloatingActionButton fabStartEdit = findViewById(R.id.fab_start_edit);
    FloatingActionButton fabEditDone = findViewById(R.id.fab_edit_done);
    TextView textViewTitle = findViewById(R.id.textViewPageTitle);
    EditText editTextTitle = findViewById(R.id.editTextPageTitle);

    // setup - start from static title with "edit" button
    fabStartEdit.setVisibility(View.VISIBLE);
    fabEditDone.setVisibility(View.GONE);
    textViewTitle.setText(currentTitle);
    textViewTitle.setVisibility(View.VISIBLE);
    editTextTitle.setText(currentTitle);
    editTextTitle.setVisibility(View.GONE);

    // handle clicks on "start edit"
    fabStartEdit.setOnClickListener(v -> {
      isEditing = true;
      fabStartEdit.setVisibility(View.GONE);
      fabStartEdit.animate()
              .alpha(0f)
              .setDuration(300l)
              .start();
      fabEditDone.setVisibility(View.VISIBLE);
      fabEditDone.animate()
              .alpha(1f)
              .setDuration(300l)
              .start();

      textViewTitle.setVisibility(View.GONE);
      editTextTitle.setVisibility(View.VISIBLE);

      //handles showing the keyboard when the edit button is pressed
      editTextTitle.requestFocus();
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.showSoftInput(editTextTitle, InputMethodManager.SHOW_IMPLICIT);

    });

    // handle clicks on "done edit"
    fabEditDone.setOnClickListener(v -> {
      isEditing = false;
      String newTitle;
      currentTitle = editTextTitle.getText().toString();
      fabStartEdit.setVisibility(View.VISIBLE);
      fabStartEdit.animate()
              .alpha(1f)
              .setDuration(300l)
              .start();

      fabEditDone.setVisibility(View.GONE);
      fabEditDone.animate()
              .alpha(0f)
              .setDuration(300l)
              .start();

      textViewTitle.setText(currentTitle);
      textViewTitle.setVisibility(View.VISIBLE);
      editTextTitle.setText(currentTitle);
      editTextTitle.setVisibility(View.GONE);

      //handles closing the keyboard when the done edit button is pressed
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(editTextTitle.getWindowToken(), 0);
    });
  }

  @Override
  public void onBackPressed() {
    // BACK button was clicked
    // find all views
    FloatingActionButton fabStartEdit = findViewById(R.id.fab_start_edit);
    FloatingActionButton fabEditDone = findViewById(R.id.fab_edit_done);
    TextView textViewTitle = findViewById(R.id.textViewPageTitle);
    EditText editTextTitle = findViewById(R.id.editTextPageTitle);

    //if the back key is pressed while the text is currently being edited
    if(isEditing) {
      fabStartEdit.setVisibility(View.VISIBLE);
      fabStartEdit.animate()
              .alpha(1f)
              .setDuration(300l)
              .start();
      fabEditDone.setVisibility(View.GONE);
      fabEditDone.animate()
              .alpha(0f)
              .setDuration(300l)
              .start();
      textViewTitle.setText(currentTitle);
      textViewTitle.setVisibility(View.VISIBLE);
      editTextTitle.setText(currentTitle);
      editTextTitle.setVisibility(View.GONE);
      isEditing = true;
    }
    else {
      super.onBackPressed();
    }
  }
}